require('dotenv').config();
const express = require('express');

const app = express();

const dbConnect = require('./app/db/mongo');

const PORT = process.env.PORT || 3000;
const Cabins = require('./app/schemas/cabinSchema');
const cabinRouter = require('./app/routes/cabinRouter')(Cabins);

dbConnect();

app.use(express.json());
app.use(express.urlencoded({
  extended: true,
}));

app.use('/api/1.0', cabinRouter);

app.listen(PORT, () => {
  console.log(`running on port: ${PORT}`);
});
