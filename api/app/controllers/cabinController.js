const httpErrors = require('../helpers/errors');
const Cabins = require('../schemas/cabinSchema');

const getAllCabins = (req, res) => {
  try {
    Cabins.find((err, findedCabins) => {
      if (err) {
        httpErrors(err);
      }
      res.json(findedCabins);
    });
  } catch (e) {
    httpErrors(e);
  }
};

const getCabin = (req, res) => {
  try {
    /*const query = {};
    if(req.query.isOccupied)*/
    Cabins.findById(req.param.cabinId, (err, cabin) => {
      if (err) {
        res.send(err);
      }
      res.json(cabin);
    });
  } catch (e) {
    httpErrors(e);
  }
};

const updateCabin = (req, res) => {
  try {
    Cabins.findById(req.param.cabinId, (err, cabin) => {
      if (err) {
        res.send(err);
      }

      cabin.name = req.body.name;
      cabin.imageUrl = req.body.imageUrl;
      cabin.description = req.body.description;
      cabin.save();

      res.json(cabin);
      // res.status(201).json(cabin);
    });
  } catch (e) {
    httpErrors(e);
  }
};

const deleteCabin = (req, res) => { };

module.exports = {
  getCabin, getAllCabins, updateCabin, deleteCabin,
};
