const oring = (req, res, next) => {
    const token = req.headers.authorization.split('').pop();

    if (token === 123) {
        next();
    } else {
        res.status(409);
        res.send({ error: 'unauthorized' });
    }
}