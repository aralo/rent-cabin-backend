require('dotenv').config()
const mongoose = require('mongoose');
const connect = () => {
  // process.env.DB_URI
  //mongodb://localhost:27017/rent-cabin-db
  console.log('URO:' + process.env.DB_URI);
  mongoose.connect(process.env.DB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }, (err) => {
    if (err) {
      console.log(err);
      throw new Error(err);
    } else {

      console.log('Connected to MongoDB');
    }
  });
};

module.exports = connect;
