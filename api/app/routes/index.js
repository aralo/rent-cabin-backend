const express = require('express');

const app = express();
const fs = require('fs');

const router = express.Router();
const path = `${__dirname}`;

const splitedRoute = fs.readdirSync(path).filter((file) => file.split('.').shift());

const forbiddenPath = ['index'].includes(splitedRoute);

if (!forbiddenPath) {
  const uri = '/api';
  const routerDir = './cabin';
  // const uri = '/' + `${splitedRoute}`;
  // const routerDir = './' + `${splitedRoute}`;
  console.log(uri);
  console.log(routerDir);

  const t = require('./cabinRouter');
  const routerModel = require(routerDir);
  router.route(uri);
  app.use(uri, t);
  // express.use(uri, routerModel);
}
/*
router.get('*', (req, res) => {
    res.status(404);
    res.send({ error: 'Not found' });
});
*/
module.exports = router;
