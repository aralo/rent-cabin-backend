const express = require('express');
const { getCabin, updateCabin, getAllCabins } = require('../controllers/cabinController');

const routes = () => {
  const router = express.Router();
  router.route('/cabin')
    .post(updateCabin)
    .get(getAllCabins);

  router.route('/cabin/:cabinId')
    .get(getCabin);

  router.route('/cabin/:cabinId')
    .patch(updateCabin);

  return router;
};

module.exports = routes;
