const httpErrors = (res, err) => {
  console.log(err);
  res.status(500);
  res.send({ error: `Server error: ${err}` });
};

module.exports = httpErrors;
