export class Cabin {
    name: String;
    imageUrl: String;
    description: String;
};