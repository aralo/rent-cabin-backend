const mongoose = require('mongoose');

const CabinScheme = new mongoose.Schema({
  name: { type: String },
  imageUrl: String,
  description: String,
});

module.exports = mongoose.model('Cabin', CabinScheme);
